import React from 'react'
import { logo } from '../../../../public'
import { Main, UnderLogoText, Logo } from './style'
import { FabButton } from '../fab/style'
import { useTranslation } from "react-i18next";

export const EmptyMainComponent = () => {
    const { t } = useTranslation()

    return (
        <Main>
            <Logo src={logo} alt="logo"/>
            <UnderLogoText>{t('basket.dashboard.emptyList.text1')}</UnderLogoText>
            <UnderLogoText>{t('basket.dashboard.emptyList.text2')}</UnderLogoText>
            <FabButton>+</FabButton>
        </Main>
    )
}
