import styled from '@emotion/styled'

export const Main = styled.main`
    width: 100%;
    display: flex;
    flex-direction: column;
    align-items: center;
    background-color: ${props => props.theme.colors.accent.light};
`;

export const Logo = styled.img`
    margin-top: 64px;
    margin-bottom: 30px;    
`;

export const UnderLogoText = styled.p`
    font-family: 'Roboto';
    font-style: normal;
    font-size: 24px;    
    color: ${props => props.theme.colors.text.light};
    margin: 0px;   
`;

