import styled from '@emotion/styled'
import { css } from '@emotion/css'
import Dropdown from 'react-dropdown'

import 'react-dropdown/style.css';

import { kebab } from '../../../../public'


export const dropdownArrow = css`
    display: none;
`

export const dropdownPlaceholderClassName = css`
    display: none;
`

export const dropdownControlClassName = css`
    background-color: inherit;
	border: inherit;
`

export const dropdownMenuClassName = css`
    overflow-y: visible;
	overflow-x: visible;
	width: auto;
`

export const CustomDropdown = styled(Dropdown)`
    width: 24px;
    height: 24px;
    cursor: pointer;
    border-radius: 50%;
    border: 0px;
    background-image: url(${kebab});
`;

export const Main = styled.main`
    width: 100%;
    display: flex;
    flex-direction: column;
    align-items: center;
    background-color: ${props => props.theme.colors.accent.light};
`;

export const ListBox = styled.div`
    width: 60%;
    display: flex;
    flex-direction: row;
    align-items: center;
    justify-content: space-between;
    margin: 10px;
    background: ${props => props.theme.colors.text.main};
    box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
`;

export const ListLeftDiv = styled.div`
    margin-left: 24px;
    margin-right: 24px;
    align-items: center;
    justify-content: start;
    word-break: break-all;
    flex-grow: 1; 
    flex-shrink: 1;
`;

export const ListRightDiv = styled.div`
    margin-right: 20px;
    flex-grow: 0; 
    flex-shrink: 0;
    justify-content: center;
    align-items: center;
    display: flex;
    flex-direction: row;
`;

export const ListText = styled.p`    
    font-family: 'Roboto';
    font-style: normal;
    font-weight: 400;
    font-size: 24px;
    line-height: 28px;
    color: ${props => props.theme.colors.bg.main};       
`;

export const ListCounts = styled.p`
    font-family: 'Roboto';
    font-style: normal;
    font-weight: 400;
    font-size: 24px;
    line-height: 28px;
    color: ${props => props.theme.colors.background.main};
    margin-right: 10px;
`;

export const Logo = styled.img`
    margin-top: 64px;
    margin-bottom: 30px;    
`;

export const UnderLogoText = styled.p`
    font-family: 'Roboto';
    font-style: normal;
    font-size: 24px;    
    color: ${props => props.theme.colors.text.light};
    margin: 0px;   
`;



