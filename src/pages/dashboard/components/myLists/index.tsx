import React from 'react'
import { useTranslation } from "react-i18next";

import { logo } from '../../../../public'
import { FabButton } from '../fab/style'

import { Main, ListBox, ListLeftDiv, ListText, ListRightDiv, ListCounts, CustomDropdown, UnderLogoText, Logo, dropdownControlClassName, dropdownPlaceholderClassName, dropdownMenuClassName, dropdownArrow } from './style'

const data = [
    {
        id: "uuid1",
        listName: "Состоялась 94-я церемония вручения премии «Оскар»: награда за лучший фильм присуждена картине...",
        purchased: 0,
        total: 5
    },
    {
        id: "uuid2",
        listName: "Второй список",
        purchased: 1,
        total: 5
    },
    {
        id: "uuid3",
        listName: "Первый список",
        purchased: 5,
        total: 5
    }
]

type MyListProps = {
    onRemove: (id: string) => void                                
}

export const MyListsComponent: React.FC<MyListProps> = ({onRemove}) => {
    const { t } = useTranslation()

    const options = [
        { value: 'Rename', label: t('basket.dashboard.dropdownmenu.rename') },
        { value: 'Share', label: t('basket.dashboard.dropdownmenu.share') },
        { value: 'Copy', label: t('basket.dashboard.dropdownmenu.copy') },
        { value: 'Delete', label: t('basket.dashboard.dropdownmenu.delete') }
    ];

    const onSelect = id => options => {
        if (options.value == 'Delete') {
            onRemove(id)
        }
    }

    return (
        <Main>
            {data?.map(listItem => (
                <ListBox key={listItem.id}>
                    <ListLeftDiv><ListText>{listItem.listName}</ListText></ListLeftDiv>
                    <ListRightDiv>
                        <ListCounts>{`${listItem.purchased} / ${listItem.total}`}</ListCounts>
                        <CustomDropdown 
                            controlClassName={dropdownControlClassName} 
                            placeholderClassName={dropdownPlaceholderClassName} 
                            menuClassName={dropdownMenuClassName}
                            arrowClassName={dropdownArrow}
                            options={options} 
                            onChange={onSelect(listItem.id)}
                        />                    
                    </ListRightDiv>
                </ListBox>
            ))}
            { !data || !data.length && (
                <>
                    <Logo src={logo} alt="logo"/>
                    <UnderLogoText>{t('basket.dashboard.emptyList.text1')}</UnderLogoText>
                    <UnderLogoText>{t('basket.dashboard.emptyList.text2')}</UnderLogoText>
                </>
            )}
            <FabButton>+</FabButton>       
        </Main>
    )
}