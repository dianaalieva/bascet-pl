import styled from '@emotion/styled'
import { kebab } from '../../../../public'
import 'react-dropdown/style.css';

export const KebabButton = styled.button`
    width: 24px;
    height: 24px;
    cursor: pointer;
    border-radius: 50%;
    border: 0px;
    background-image: url(${kebab}); 
        
`;
