import React from 'react'
import { Header, HeaderText } from './style'
import { useTranslation } from "react-i18next";

export const HeaderComponent = () => {
    const { t } = useTranslation()

    return (
        <Header>
            <HeaderText>{t('basket.dashboard.header.title')}</HeaderText>      
        </Header>
    )
}
