import styled from '@emotion/styled'

export const Header = styled.header`
    height: 64px;
    display: flex;
    align-items: center;
    justify-content: center;
    background-color: ${props => props.theme.colors.background.main};
`;

export const HeaderText = styled.p`
    font-family: 'Roboto';
    font-style: bold;
    font-size: 28px;    
    color: ${props => props.theme.colors.text.main};    
`;
