import React from 'react'
import { useTranslation } from "react-i18next";
import { Main, FormDiv, Form, InputDiv, CreateListLabel, Input, CancelButton, CreateButton, ButtonDiv } from './style'


export const CreateListComponent = () => {
    const { t } = useTranslation()

    return (
        <Main>
            <FormDiv>
                <Form>
                    <InputDiv>
                        <CreateListLabel htmlFor="name">{t('basket.dashboard.createlistform.listname')}</CreateListLabel>
                        <Input type="text" id="name" placeholder={t('basket.dashboard.createlistform.placeholder')}/>
                    </InputDiv>
                    <ButtonDiv>
                        <CancelButton>{t('basket.dashboard.createlistform.cancelbutton')}</CancelButton>
                        <CreateButton type="submit">{t('basket.dashboard.createlistform.createbutton')}</CreateButton>
                    </ButtonDiv>
                </Form>
            </FormDiv>
        </Main>
    )
}

type DeleteListProps = {
    onCancel: () => void                                
}

export const DeleteListComponent: React.FC<DeleteListProps> = ({onCancel}) => {
    const { t } = useTranslation()

    return (
        <Main>
            <FormDiv>
                <Form>
                    <InputDiv>
                        <CreateListLabel htmlFor="name">{t('basket.dashboard.deletelistform.question')}</CreateListLabel>                        
                    </InputDiv>
                    <ButtonDiv>
                        <CancelButton onClick={onCancel}>{t('basket.dashboard.createlistform.cancelbutton')}</CancelButton>
                        <CreateButton type="submit">{t('basket.dashboard.deletelistform.okbutton')}</CreateButton>
                    </ButtonDiv>
                </Form>
            </FormDiv>
        </Main>
    )
}