import React, { useState } from "react";
import styled from "@emotion/styled";

import '../../style';

import { HeaderComponent, EmptyMainComponent, MyListsComponent, CreateListComponent, DeleteListComponent } from './components';

// const TopWrapper = styled.div`
//     display: flex;
//     position: fixed;
//     width: 100%;
//     height: 100%;
        
// `;

const TopWrapper2 = styled.div`
    display: flex;
    width: 100%;    
    flex-direction: column;               
`;

// export const DashboardPage1 = () => {
//     return (
//         <>              
//             <HeaderComponent/>
//             <TopWrapper>
//                 <EmptyMainComponent/>                                
//             </TopWrapper>
//         </> 
//     )
// }

// export const DashboardPage2 = () => {
//     return (
//         <>              
//             <HeaderComponent/>
//             <TopWrapper>
//                 <CreateListComponent/>
//             </TopWrapper>                                             
            
//         </> 
//     )
// }

// export const DashboardPage3 = () => {
//     return (
//         <>              
//             <HeaderComponent/>
//             <TopWrapper>
//                 <MyListsComponent/>                                
//             </TopWrapper>
//         </> 
//     )
// }

export const DashboardPage = () => {
    const [toRemove, setToRemove] = useState(null)

    return (
        <>              
            <HeaderComponent/>
            <TopWrapper2>
                <EmptyMainComponent/>
                <CreateListComponent/>
                {toRemove && <DeleteListComponent onCancel={() => setToRemove(null)}/>}
                {!toRemove && <MyListsComponent onRemove={setToRemove}/>}                              
            </TopWrapper2>
        </> 
    )
}

