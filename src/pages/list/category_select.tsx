import React, { useState } from "react";

import { Header2 } from "./components/headers";
import { CategoryItem } from "./components/category";

import { 
    Container,
    ListHeadName,
    Categories,
    Comment,
    InputName,
    Input,
    InputNameText,
    InputColor,
    AddCategory,
    InputTextWrap,
    InputText,
    Textarea,
    Add
} from "../list/style";

export const CategorySelect = () => {
    const [categoryName, setCategoryName] = useState("");
    const handleCategoruNameChange = (event) => {
        const value = event.target.value;
        setCategoryName(value)
    }

    return (
        <div>
            <Header2/>
            <Container>
                <ListHeadName>Выберите категорию:</ListHeadName>
                <Categories>
                    <CategoryItem categoryName="Кисломолочная продукция"/>
                    <CategoryItem categoryName="Хлебобулочные изделия"/>
                    <CategoryItem categoryName="Бытовая химия"/>
                    <CategoryItem categoryName="Для животных"/>
                    <CategoryItem categoryName="Одежда"/>
                    <CategoryItem categoryName="Продукты"/>
                    <CategoryItem categoryName="Для дома"/>
                    <CategoryItem categoryName="Лекарства"/>
                </Categories>
                <Comment>Если нет подходящей категории:</Comment>
                <form>
                    <InputName>
                        <Input type="text" name="category_name" value={categoryName} onChange={handleCategoruNameChange}/>
                        <InputNameText>Введите название категории</InputNameText>
                    </InputName>
                    <InputColor>
                        <option value="none"disabled selected>Выберите цвет</option>
                        <option value="#08AE0F">#08AE0F</option>
                        <option value="#9D79B9">#9D79B9</option>
                        <option value="#B11F1F">#B11F1F</option>
                        <option value="#3414F5">#3414F5</option>
                        <option value="#008060">#008060</option>
                        <option value="#D928D2">#D928D2</option>
                        <option value="#9B763D">#9B763D</option>
                        <option value="#830DF9">#830DF9</option>
                        <option value="#FA8803">#FA8803</option>
                        <option value="#FCF20E">#FCF20E</option>
                        <option value="#EC1818">#EC1818</option>
                        <option value="#14F52B">#14F52B</option>
                    </InputColor>
                    <AddCategory>Добавить категорию</AddCategory>
                    <InputTextWrap>
                        <InputText>Введите текст</InputText>
                        <Textarea name="item_text"></Textarea>
                    </InputTextWrap>
                    <Add>Добавить</Add>
                </form>
            </Container> 
        </div>
    )
}