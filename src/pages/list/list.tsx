import React from "react";
import { Header1 } from "./components/headers";
import { Item,ItemBought } from "./components/item";

import { FabButton } from "../dashboard/components/fab/style";

import { 
    Container,
    ListHeadName,
} from "./style";

export const List = () => {
    return (
        <div>
            <Header1/>
            <Container>
                <div>
                    <ListHeadName>Список 1</ListHeadName>
                    <Item/>
                    <Item/>
                    <Item/>
                    <Item/>
                    <ItemBought/>
                    <ItemBought/>
                    <FabButton>+</FabButton>
                </div>
            </Container>
        </div>
    )
}

