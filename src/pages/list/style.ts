import styled from "@emotion/styled";

export const Btn = styled.button `
    background: transparent;
    border: transparent;
    width: min-content;
`;

export const ArrowLeftWrap = styled.div `
    display: inline-block;
`;

export const RotateWrap = styled.div `
    display: inline-block;
    padding-left: 1.8rem;
`;

export const ShareWrap = styled.div `
    display: inline-block;
    right: 1.2rem;
    position: absolute;
`;

export const BsktArrWrap = styled.div `
    margin-left: 0.3rem;
`;

export const BsktArrDown = styled.div `
    margin-left: 0.3rem;
    transform: rotate(180deg);
`;

export const Container = styled.div `
    padding: 1.2rem;
    position: relative;
`;

export const Header = styled.div `
    background-color: #008060;
`;

export const ListHeadName  = styled.h2 `
    margin-bottom: 1.2rem;
    font-family: Roboto;
    font-size: 1.2rem;
    font-style: normal;
    font-weight: 400;
    line-height: 1.4rem;
    letter-spacing: 0em;
    text-align: left;
    color: #42474C;
`;

export const ListItem = styled.div `
    display: flex;
    height: 6.25rem;
    box-shadow: 0px 0.5rem 0.5rem 0px rgba(2, 2, 2, 0.25);
    border-radius: 0.5rem;
    margin-bottom: 0.6rem;
`;

export const ListItemBlock1 = styled.div `
    flex-grow: 1;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    background-color: #08AE0F;
    background-size: contain;
    border-bottom-left-radius: 0.5rem;
    border-top-left-radius: 0.5rem;
`;

export const ItmBlck1Bought = styled.div `
    flex-grow: 1;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    background-color: #08AE0F1A;
    background-size: contain;
    border-bottom-left-radius: 0.5rem;
    border-top-left-radius: 0.5rem;
`;

export const ListItemBlock2 = styled.div `
    flex-grow: 7;
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    padding: 0.4rem;
`;

export const ItemText = styled.p `
    font-family: Roboto;
    font-size: 0.8rem;
    font-weight: 500;
    line-height: 0.9rem;
    letter-spacing: 0em;
    text-align: left;
    color: #42474C;
`;

export const ItemTextBought = styled.p `
    font-family: Roboto;
    font-size: 0.8rem;
    font-weight: 500;
    line-height: 0.9rem;
    letter-spacing: 0em;
    text-align: left;
    color: #42474C;
    text-decoration: line-through;
`;

export const ListItemBlock3 = styled.div `
    flex-grow: 1;
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content:space-around;
    background-color: #08AE0F1A;
    border-bottom-right-radius: 0.5rem;
    border-top-right-radius: 0.5rem;
`;


export const Categories = styled.div `
    width: 100%;
    display: grid;
    grid-template-columns: repeat(2,1fr);
    grid-gap: 0.5rem;
    margin-bottom: 0.5rem;
`;

export const Category = styled.div `
    display: flex;
    flex-direction: row;
    height: 3rem;
    border-radius: 0.2rem;
    box-shadow: 0px 0.2rem 0.2rem 0px rgba(2, 2, 2, 0.25);
`;

export const CategoryColor = styled.div `
    width: 1rem;
    height: inherit;
    background-color: #08AE0F;
    border-bottom-left-radius: 0.2rem;
    border-top-left-radius: 0.2rem;
`;

export const CategoryName = styled.div `
    padding: 0.2rem;
`;

export const CategoryNameText = styled.div `
    font-family: Roboto;
    font-size: 0.8rem;
    font-style: normal;
    font-weight: 500;
    line-height: 0.95rem;
    letter-spacing: 0em;
    color: #42474C;
`;

export const Comment = styled.div `
    font-family: Roboto;
    font-size: 12px;
    font-style: normal;
    font-weight: 500;
    line-height: 14px;
    letter-spacing: 0em;
    text-align: left;
    color: #9828F0;
    margin-bottom: 0.5rem;
`;

export const InputName = styled.div `
    width: 100%;
    position: relative;
    display: block;
    border-radius: 0.2rem;
    box-shadow: 0px 0.2rem 0.2rem 0px rgba(2, 2, 2, 0.25);
    margin-bottom: 0.5rem;
`;

export const Input = styled.input `
    width: 100%;
    box-sizing: border-box;
    border-radius: 0.2rem;
    font-family: Roboto;
    font-size: 18px;
    font-style: normal;
    font-weight: 500;
    line-height: 21px;
    letter-spacing: 0em;
    text-align: left;
    color: #6B7177;
    border: transparent;
    padding: 0.5rem;
`;

export const InputNameText = styled.p `
    position: absolute;
    top: -20%;
    left: 0.5rem;
    color:#6B717780;
    font-family: Roboto;
    font-size: 18px;
    font-style: normal;
    font-weight: 500;
    line-height: 21px;
    letter-spacing: 0em;
    text-align: center;
`;

export const InputColor = styled.select `
    width: 100%;
    border-radius: 0.2rem;
    font-family: Roboto;
    font-size: 18px;
    font-style: normal;
    font-weight: 500;
    line-height: 21px;
    letter-spacing: 0em;
    text-align: left;
    color: #6B7177;
    border: transparent;
    padding: 0.5rem;
    box-shadow: 0px 0.2rem 0.2rem 0px rgba(2, 2, 2, 0.25);
    margin-bottom: 0.5rem;
`;

export const AddCategory = styled.button `
    width: 100%;
    height: 2.5rem;
    background-color: #F6EAFF;
    border-radius: 0.2rem;
    border: transparent;
    box-shadow: 0px 0.2rem 0.2rem 0px rgba(2, 2, 2, 0.25);
    margin-bottom: 1rem;
    font-family: Roboto;
    font-size: 18px;
    font-style: normal;
    font-weight: 500;
    line-height: 21px;
    letter-spacing: 0em;
    text-align: center;
`;

export const InputTextWrap = styled.div `
    position: relative;
    width: 100%;
    display: block;
    margin-bottom: 0.5rem;
`;

export const InputText = styled.p `
    position: absolute;
    top: 10%;
    left: 0.5rem;
    color:#6B717780;
    font-family: Roboto;
    font-size: 18px;
    font-style: normal;
    font-weight: 500;
    line-height: 21px;
    letter-spacing: 0em;
    text-align: center;
`;

export const Textarea = styled.textarea `
    width: 100%;
    box-sizing: border-box;
    border-radius: 0.2rem;
    font-family: Roboto;
    font-size: 18px;
    font-style: normal;
    font-weight: 500;
    line-height: 21px;
    letter-spacing: 0em;
    text-align: left;
    color: #6B7177;
    border: transparent;
    padding: 0.5rem;
    box-shadow: 0px 0.2rem 0.2rem 0px rgba(2, 2, 2, 0.25);
`;

export const Add = styled.button `
    width: 100%;
    height: 2.5rem;
    background-color: #74D14C;
    border-radius: 0.2rem;
    border: transparent;
    box-shadow: 0px 0.2rem 0.2rem 0px rgba(2, 2, 2, 0.25);
    margin-bottom: 1rem;
    font-family: Roboto;
    font-size: 18px;
    font-style: normal;
    font-weight: 500;
    line-height: 21px;
    letter-spacing: 0em;
    text-align: center;
`
