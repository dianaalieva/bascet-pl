import React from "react";

import { 
    basket_arrow,
    basket,
    del,
    plus,
    minus 
} from '../../../public';


import { 
    ListItem,
    ListItemBlock1,
    ListItemBlock2,
    ListItemBlock3,
    BsktArrWrap,
    BsktArrDown,
    ItemText,
    ItemTextBought,
    ItmBlck1Bought,
    Btn

} from "../style";

export const Item = () => {
    return(
        <ListItem>
            <ListItemBlock1>
                <Btn>
                    <BsktArrWrap>
                        <img src={basket_arrow} alt=""/>
                    </BsktArrWrap>
                    <img src={basket} alt=""/>
                </Btn>
            </ListItemBlock1>
            <ListItemBlock2>
                <ItemText>Курица-гриль</ItemText>
                <Btn>
                    <img src={del} alt=""/>
                </Btn>
            </ListItemBlock2>
            <ListItemBlock3>
                <Btn>
                    <img src={plus} alt=""/>
                </Btn>
                <div>1</div> 
                <Btn>
                    <img src={minus} alt=""/>
                </Btn>
            </ListItemBlock3>
        </ListItem>
    )
}

export const ItemBought = () => {
    return (
        <ListItem>
            <ItmBlck1Bought>
                <Btn>
                    <BsktArrDown>
                        <img src={basket_arrow} alt=""/>
                    </BsktArrDown>
                    <img src={basket} alt=""/>
                </Btn>
            </ItmBlck1Bought>
            <ListItemBlock2>
                <ItemTextBought>Курица-гриль</ItemTextBought>
                <Btn>
                    <img src={del} alt=""/>
                </Btn>
            </ListItemBlock2>
            <ListItemBlock3>
                <Btn>
                    <img src={plus} alt=""/>
                </Btn>
                <div>1</div> 
                <Btn>
                    <img src={minus} alt=""/>
                </Btn>
            </ListItemBlock3>
        </ListItem>
    )
}
