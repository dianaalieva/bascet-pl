import React from "react";

import {
    Category,
    CategoryColor,
    CategoryName,
    CategoryNameText,
} from '../style'

type CategoryItemProps = {
    categoryName: string;
}

export const CategoryItem: React.FC<CategoryItemProps> = ({ categoryName }) => {
    return(
        <Category>
            <CategoryColor/>
            <CategoryName>
                <CategoryNameText>{categoryName}</CategoryNameText>
            </CategoryName>
        </Category>
    )
}