import React from "react";

import { 
    arrow_left,
    rotate,
    share
} from "../../../public";

import { 
    Container,
    Header,
    RotateWrap,
    ShareWrap,
    ArrowLeftWrap,
    Btn

} from "../style";

export const Header1 = () => (
    <Header>
        <Container>
            <ArrowLeftWrap>
                <Btn>
                    <img src={arrow_left} alt=""/>
                </Btn>
            </ArrowLeftWrap>
            <RotateWrap>
                <Btn>
                    <img src={rotate} alt=""/>
                </Btn>
            </RotateWrap>
            <ShareWrap>
                <Btn>
                    <img src={share} alt=""/>
                </Btn>
            </ShareWrap>
        </Container>
    </Header>
)

export const Header2 = () => (
    <Header>
        <Container>
            <Btn>
                <img src={arrow_left} alt=""/>
            </Btn>
        </Container>
    </Header> 
)