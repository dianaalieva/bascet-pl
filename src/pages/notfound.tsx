import React from "react";
import Lottie from "lottie-react";
import { animations } from '../__data__';
import styled from "@emotion/styled";
import { useTranslation } from "react-i18next";

const TopWrapper = styled.div`
    display: flex;
    align-items: center;
    flex-direction: column;
    width: 100%;
    height: 100%;   
`;

const AniWrapper = styled.div`
    width: 50%;
    height: 50%;
`;

export const NotFoundPage = () => {
    
    const { t } = useTranslation()

    return (
        <>  
            <TopWrapper>
                <h1>{t('basket.notfoundpage.title')}</h1>
                <AniWrapper>          
                    <Lottie animationData={animations.oopss}/>
                </AniWrapper>
            </TopWrapper>            
        </> 
    )
}
