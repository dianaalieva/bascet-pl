import React from "react";

import { Routes, Route } from "react-router-dom";
import { URLs } from "./__data__/urls";
import { MainLandingComponent as Main } from './pages/main_landing';
import { DashboardPage } from './pages/dashboard/dashboard';
import { NotFoundPage } from './pages/notfound';
import { SignIn } from './pages/registration/sign-in';
import { SignUp } from './pages/registration/sign-up';
import { ResetPassword } from './pages/registration/reset-password';
import { List } from "./pages/list/list";
import { CategorySelect } from "./pages/list/category_select";

export const Routing = () => (
    <Routes>
      
      {URLs.landing.isEnabled && <Route path={URLs.landing.url} element={<Main />}/>}    
      {URLs.signIn.url && <Route path={URLs.signIn.url} element={<SignIn />}/>}
      {URLs.signUp.url && <Route path={URLs.signUp.url} element={<SignUp />}/>}
      {URLs.resetPassword.url && <Route path={URLs.resetPassword.url} element={<ResetPassword/> }/>}
      {URLs.dashboard.url && <Route path={URLs.dashboard.url} element={<DashboardPage />}/>}
      {URLs.list.url && <Route path={URLs.list.url} element={<List />}/>}
      {URLs.add.url && <Route path={URLs.add.url} element={<CategorySelect />}/>}
      <Route path="*" element={<NotFoundPage/>}/>
    </Routes>
)