import React from 'react';
import { BrowserRouter } from "react-router-dom";
import { Routing } from './routing';
import { ThemeProvider } from '@emotion/react'
import { theme } from './theme'
import './style';

const App = () => {
    return(
        <ThemeProvider theme={theme}>
            <BrowserRouter>
                <Routing/>
            </BrowserRouter>
        </ThemeProvider>
    )
}

export default App;