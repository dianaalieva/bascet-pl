export const theme = {
    colors: {
        brand: {
            light: '#74D14C',
            main: '#74D14C',
            dark: '#54AB26'
        },
        background: {
            main: '#008060'
        },
        accent: {
            light3: '#FAF9F8',
			light2: '#D2D5D9',
			light: '#F6EAFF',
            main: '#9828F0',
            dark: '#2C107A'
        },
        text: {
            light2: '#42474C',
            light: '#6B7177',
            main: '#FFFFFF',
            dark: '#000000',
        },
        bg: {
            main: '#000000'
        }
    }
  }
