const pkg = require('./package')

module.exports = {
    apiPath: 'stubs/api',
    webpackConfig: {
        output: {
            publicPath: `/static/${pkg.name}/${process.env.VERSION || pkg.version}/`
        }
    },
    navigations: {
        "basket.main": "/basket",
        "basket.sign.in": "/sign-in",
        "basket.sign.up": "/sign-up",
        "basket.dashboard": "/dashboard",
        "basket.list": "/list/:id",
        "basket.add": "/add",
        "basket.reset.password": "/reset/password"
    },
    features: {
        'bascet-pl': {
            // add your features here in the format [featureName]: { value: string }
        },
    },
    config: {
        key: 'value'
    }
}
